
function ArrowSpheres_2(resources)
{
	ArrowSpheres_2.resources = resources;
}
ArrowSpheres_2.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(691, 400, Phaser.CANVAS, 'arrow_spheres_2', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 691;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
		//this.game.load.image('placeholder', ArrowSpheres_2.resources.placeholder);
        this.game.load.image('lineWork', ArrowSpheres_2.resources.lineWork);
       
		
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			//this.parent.background = this.game.add.sprite(0,0,'placeholder');
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
        
        
        
	},
    
	buildAnimation: function()
	{
        this.spherePositions = [{x:105,y:68},{x:268,y:68},{x:430,y:68},{x:590,y:68}];
		var style = ArrowSpheres_2.resources.textStyle_1;
        var style2 = ArrowSpheres_2.resources.textStyle_2;
        this.spheresGroup = this.game.add.group();
        //this.sphereGroupsAr = [];
        this.spheresGroup.x = 0;
        this.spheresGroup.y = 0;
        
        var testGroup = this.game.add.group();
        for(var i = 0; i<this.spherePositions.length; i++)
        {
            var temp = this.game.make.sprite(0,0);
            temp.id = i;
            temp.anchor.set(0.5,0.5);
            temp.addChild(this.drawSphere());
            temp.x = this.spherePositions[i].x;
            temp.y = this.spherePositions[i].y;
            
            //
            temp.sphereText = this.game.add.text(0,0,ArrowSpheres_2.resources["sphereText_"+(i+1)],style); 
            temp.addChild(temp.sphereText);
            temp.sphereText.anchor.set(0.5,0.5);
            temp.sphereText.y+=5;
            temp.arrow_1 = this.drawArrow();
            temp.arrow_1.x =-5;
            temp.arrow_1.y =60;
            temp.arrow_1.anchor.set(0.5,0.5);
            temp.addChild(temp.arrow_1);
            temp.arrow_1.alpha = 0;
            temp.anim_arrow_1 = this.game.add.tween(temp.arrow_1).to({alpha:1,y:70},400,Phaser.Easing.Quadratic.Out,true,1000);
            
            temp.textLine_1 = this.game.add.text(0,0,ArrowSpheres_2.resources["textLine_"+(i+1)],style2); 
            temp.addChild(temp.textLine_1);
            temp.textLine_1.anchor.set(0.5,0.5);
            temp.textLine_1.y = temp.arrow_1.y+50;
            temp.textLine_1.alpha = 0; 
            temp.anim_textLine_1 = this.game.add.tween(temp.textLine_1).to({alpha:1,y:temp.arrow_1.y+65},400,Phaser.Easing.Quadratic.Out,true,2000);
            
            //
            temp.arrow_2 = this.drawArrow();
            temp.arrow_2.x =-5;
            temp.arrow_2.y =135;
            temp.arrow_2.alpha = 0;
            temp.arrow_2.anchor.set(0.5,0.5);
            temp.addChild(temp.arrow_2);
            temp.anim_arrow_2 = this.game.add.tween(temp.arrow_2).to({alpha:1,y:145},400,Phaser.Easing.Quadratic.Out,true,3000);
            //
            temp.textLine_2 = this.game.add.text(0,0,ArrowSpheres_2.resources["textLine_"+(i+5)],style2); 
            temp.addChild(temp.textLine_2);
            temp.textLine_2.alpha = 0;
            temp.textLine_2.anchor.set(0.5,0.5);
            temp.textLine_2.y = 190;
            temp.anim_textLine_2 = this.game.add.tween(temp.textLine_2).to({alpha:1,y:200},400,Phaser.Easing.Quadratic.Out,true,4000);
            
            this.lineWork = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY+120,'lineWork');
            this.lineWork.anchor.set(0.5,0.5);
            this.lineWork.alpha = 0;
            
            
            
            this.spheresGroup.add(temp);
            temp.anim_1 = this.game.add.tween(temp).to({alpha:1,},1000,Phaser.Easing.Quadratic.Out);
            temp.alpha = 0;
        }
        this.footerText = this.game.add.text(this.lineWork.x,this.lineWork.y+55,ArrowSpheres_2.resources.footerLine,ArrowSpheres_2.resources.textStyle_2);
            this.footerText.alpha = 0;
            this.footerText.anchor.set(0.5,0.5);
            this.anim_linework = this.game.add.tween(this.lineWork).to({alpha:1,},1000,Phaser.Easing.Quadratic.Out,true,5000);
            this.anim_footer_text = this.game.add.tween(this.footerText).to({alpha:1,y:this.game.world.centerY+175},1000,Phaser.Easing.Quadratic.Out,true,6000);
            this.lineWork.alpha = 0;
        for(var i = 0; i<4; i++)
        {
            this.getSphere(i).anim_1.start();
            //this.getSphere(i).anim_1.onComplete.add(this.getSphere(i).anim_arrow_1.start, this);
            this.getSphere(i).anim_arrow_1.start();
            this.getSphere(i).anim_textLine_1.start();
            this.getSphere(i).anim_arrow_2.start();
            this.getSphere(i).anim_textLine_2.start();
        }
        this.anim_linework.start();
        this.anim_footer_text.start();
        /* this.getSphere(0).anim_1.chain(this.getSphere(1).anim_1,this.getSphere(2).anim_1,this.getSphere(3).anim_1,this.getSphere(0).anim_arrow_1,this.getSphere(1).anim_arrow_1,this.getSphere(2).anim_arrow_1,this.getSphere(3).anim_arrow_1);
        this.getSphere(0).anim_1.start();
        */
    },
    getSphere: function(id)
    {
       return this.spheresGroup.filter(function(child, index, children) {return child.id == id}, true).list[0]; 
    },
    
    drawSphere: function()
    {
        var graphics = this.game.make.graphics(0,0);
        graphics.beginFill(ArrowSpheres_2.resources.fillColor, 1);
        graphics.lineStyle(1,ArrowSpheres_2.resources.strokeColor);
        graphics.drawCircle(0,0, 110);
        return graphics
    },
    
    drawArrow: function(x,y)
    {
        var graphics = this.game.make.graphics(0,0);
        graphics.beginFill(ArrowSpheres_2.resources.fillColor, 1);
        graphics.lineStyle(1, ArrowSpheres_2.resources.strokeColor);
        graphics.moveTo(0,0);
        graphics.lineTo(0,25);
        graphics.lineTo(-5,25);
        graphics.lineTo(5,35);
        graphics.lineTo(15,25);
        graphics.lineTo(10,25);
        graphics.lineTo(10,0);
        
        return graphics
    },
    
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/
var _resources= 
{
    "textStyle_1":{"font":"18px freight-sans-pro","fill": "#ffffff","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -5 },
    "textStyle_2":{"font":"16px freight-sans-pro","fill": "#000000","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -5 },
    //"placeholder":"/arrow_spheres/placeholder.png",
    "lineWork":"/arrow_spheres_2/linework.png",
    "fillColor":"0x5e9cd3",
    "strokeColor":"0x42739c",
    "sphereText_1": "Person\n5",
    "sphereText_2": "Person\n6",
    "sphereText_3": "Person\n7",
    "sphereText_4": "Person\n7",
    "textLine_1":"June 2: Search",
    "textLine_2":"June 2: Search",
    "textLine_3":"June 2: Search",
    "textLine_4":"June 2: Search",
    "textLine_5":"June 2: Add to Cart",
    "textLine_6":"June 4: Add to Cart",
    "textLine_7":"June 5: Add to Cart",
    "textLine_8":"June 9: Add to Cart",
    "footerLine":"COHORT"
}
var start = new ArrowSpheres_2(_resources).init();
//5e9cd3
//stroke//42739c


